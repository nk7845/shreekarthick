import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('navItemAnimation', [
      state(
        'active',
        style({
          transform: 'rotateY(360deg)',
        })
      ),
      transition('* => active', animate('300ms ease-in-out')),
      transition('active => *', animate('300ms ease-in-out')),
    ]),
  ],
})
export class NavbarComponent {
  navbarOpen = false;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
